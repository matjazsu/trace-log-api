build:	##  rebuilds docker image
	docker-compose build

run:	## runs whole stack with docker-compose
	docker-compose up --force-recreate -d

stop:	## stops the whole stack
	docker-compose stop

kill:	## kills the whole stack
	docker-compose kill

remove:	## removes all containers belonging to the stack
	docker-compose rm

migrate:
	docker-compose exec server ./manage.py migrate

test_server:
	docker-compose exec server pytest
