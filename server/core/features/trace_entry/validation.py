import datetime

from . import exceptions
from . import constants


class TraceEntryValidationMixin(object):
    def clean(self):
        super().clean()
        self._validate_timestamp()
        self._validate_type()

    def _validate_timestamp(self):
        if self.timestamp is None:
            raise exceptions.InvalidTimestamp
        if not isinstance(self.timestamp, int):
            raise exceptions.InvalidTimestamp

    def _validate_type(self):
        if self.type is None:
            raise exceptions.InvalidType
        if self.type not in (
                constants.TraceType.TRACE_START,
                constants.TraceType.TRACE_END,
                constants.TraceType.SPAN_START,
                constants.TraceType.SPAN_END,
                constants.TraceType.EVENT
        ):
            raise exceptions.InvalidType
