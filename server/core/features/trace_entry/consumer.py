import json

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer

from . import service


class TraceEntryConsumer(AsyncWebsocketConsumer):
    trace_id: str

    async def connect(self):
        await super().connect()
        self.trace_id = self.scope["url_route"]["kwargs"]["trace_id"]

    async def websocket_receive(self, message):
        if "text" in message:
            trace_entries = json.loads(message["text"])
            processed = await self.store_trace_entries(self.trace_id, trace_entries)
            await self.send(text_data=json.dumps({
                "processed": processed,
                "data": trace_entries,
            }))

    @database_sync_to_async
    def store_trace_entries(self, trace_id, trace_entries):
        return service.store_trace_entries(trace_id, trace_entries)
