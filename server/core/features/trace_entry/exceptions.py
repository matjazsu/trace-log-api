from common.errors_base import BaseError


class InvalidTraceEntry(BaseError):
    pass


class InvalidTimestamp(BaseError):
    pass


class InvalidType(BaseError):
    pass


