class TraceType(object):
    TRACE_START = "TRACE_START"
    TRACE_END = "TRACE_END"
    SPAN_START = "SPAN_START"
    SPAN_END = "SPAN_END"
    EVENT = "EVENT"
