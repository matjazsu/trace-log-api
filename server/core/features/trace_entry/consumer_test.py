import pytest
from channels.testing import WebsocketCommunicator
from channels.routing import URLRouter
from django.urls import path

from . import consumer


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_trace_entry_consumer():
    application = URLRouter([
        path("api/trace/<str:trace_id>", consumer.TraceEntryConsumer),
    ])
    communicator = WebsocketCommunicator(application, "/api/trace/123TEST123")
    connected, _ = await communicator.connect()
    assert connected

    data = [{
        "timestamp": 1569972082,
        "type": "TRACE_START",
        "group_id": "e6fa79ca-d142-4046-a134-5134f16a0b5e",
    }]
    await communicator.send_json_to(data)

    response = await communicator.receive_json_from()
    assert response == {
        "processed": True,
        "data": data,
    }

    await communicator.disconnect()


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_trace_entry_consumer_invalid_timestamp():
    application = URLRouter([
        path("api/trace/<str:trace_id>", consumer.TraceEntryConsumer),
    ])
    communicator = WebsocketCommunicator(application, "/api/trace/123TEST123")
    connected, _ = await communicator.connect()
    assert connected

    data = [{
        "timestamp": None,
        "type": "TRACE_START",
        "group_id": "e6fa79ca-d142-4046-a134-5134f16a0b5e",
    }]
    await communicator.send_json_to(data)

    response = await communicator.receive_json_from()
    assert response == {
        "processed": False,
        "data": data,
    }

    await communicator.disconnect()


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_trace_entry_consumer_invalid_type():
    application = URLRouter([
        path("api/trace/<str:trace_id>", consumer.TraceEntryConsumer),
    ])
    communicator = WebsocketCommunicator(application, "/api/trace/123TEST123")
    connected, _ = await communicator.connect()
    assert connected

    data = [{
        "timestamp": 1569972082,
        "type": "INVALID_TYPE",
        "group_id": "e6fa79ca-d142-4046-a134-5134f16a0b5e",
    }]
    await communicator.send_json_to(data)

    response = await communicator.receive_json_from()
    assert response == {
        "processed": False,
        "data": data,
    }

    await communicator.disconnect()
