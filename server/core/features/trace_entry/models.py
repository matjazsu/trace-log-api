from django.db import models

from . import validation


class TraceEntry(validation.TraceEntryValidationMixin, models.Model):
    trace_id = models.CharField(max_length=256)
    timestamp = models.IntegerField()
    type = models.CharField(max_length=32)
    group_id = models.CharField(max_length=256, null=True, blank=True)
    name = models.CharField(max_length=256, null=True, blank=True)
    archived = models.BooleanField(default=False)

    objects = models.Manager

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)
