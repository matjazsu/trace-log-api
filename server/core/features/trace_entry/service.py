from django.db import transaction

from . import models
from . import exceptions


def store_trace_entries(trace_id, entries):
    if entries is None or not isinstance(entries, list):
        return False
    try:
        items = [create_trace_entry_from_dict(trace_id, entry_dict) for entry_dict in entries]
        return len(items) == len(entries)
    except (exceptions.InvalidTraceEntry, exceptions.InvalidTimestamp, exceptions.InvalidType):
        return False


@transaction.atomic()
def create_trace_entry_from_dict(trace_id, data_dict):
    if data_dict is None or not isinstance(data_dict, dict):
        raise exceptions.InvalidTraceEntry
    entry = models.TraceEntry(
        trace_id=trace_id,
        timestamp=data_dict.get("timestamp"),
        type=data_dict.get("type"),
        group_id=data_dict.get("group_id"),
        name=data_dict.get("name"),
    )
    entry.save()
    return entry


@transaction.atomic()
def archive_trace_entries(entries):
    if entries is None or len(entries) == 0:
        return
    for entry in entries:
        entry.archived = True
        entry.save()
