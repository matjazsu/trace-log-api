from django.test import TestCase
from mixer.backend.django import mixer

from . import service
from . import constants
from . import exceptions
from . import models
from . import constants


class ServiceTestCase(TestCase):
    def test_create_trace_entry_from_dict(self):
        trace_id = "123TEST123"
        data_dict = {
            "timestamp": 1569972082,
            "type": "TRACE_START",
            "group_id": "e6fa79ca-d142-4046-a134-5134f16a0b5e",
        }
        entry = service.create_trace_entry_from_dict(trace_id, data_dict)
        self.assertIsNotNone(entry)
        self.assertEqual(entry.trace_id, trace_id)
        self.assertEqual(entry.timestamp, 1569972082)
        self.assertEqual(entry.type, constants.TraceType.TRACE_START)
        self.assertEqual(entry.group_id, "e6fa79ca-d142-4046-a134-5134f16a0b5e")
        self.assertIsNone(entry.name)
        self.assertFalse(entry.archived)

    def test_create_trace_entry_from_dict_invalid_timestamp(self):
        trace_id = "123TEST123"
        data_dict = {
            "timestamp": None,
            "type": "TRACE_START",
            "group_id": "e6fa79ca-d142-4046-a134-5134f16a0b5e",
        }
        with self.assertRaises(exceptions.InvalidTimestamp):
            service.create_trace_entry_from_dict(trace_id, data_dict)

        data_dict["timestamp"] = "INVALID"
        with self.assertRaises(exceptions.InvalidTimestamp):
            service.create_trace_entry_from_dict(trace_id, data_dict)

    def test_create_trace_entry_from_dict_invalid_type(self):
        trace_id = "123TEST123"
        data_dict = {
            "timestamp": 1569972082,
            "type": None,
            "group_id": "e6fa79ca-d142-4046-a134-5134f16a0b5e",
        }
        with self.assertRaises(exceptions.InvalidType):
            service.create_trace_entry_from_dict(trace_id, data_dict)

        data_dict["type"] = "INVALID"
        with self.assertRaises(exceptions.InvalidType):
            service.create_trace_entry_from_dict(trace_id, data_dict)

    def test_create_trace_entry_from_invalid_dict(self):
        trace_id = "123TEST123"
        with self.assertRaises(exceptions.InvalidTraceEntry):
            self.assertIsNone(service.create_trace_entry_from_dict(trace_id, None))
        with self.assertRaises(exceptions.InvalidTraceEntry):
            self.assertIsNone(service.create_trace_entry_from_dict(trace_id, []))
        with self.assertRaises(exceptions.InvalidTraceEntry):
            self.assertIsNone(service.create_trace_entry_from_dict(trace_id, 123))
        with self.assertRaises(exceptions.InvalidTraceEntry):
            self.assertIsNone(service.create_trace_entry_from_dict(trace_id, ""))
        with self.assertRaises(exceptions.InvalidTraceEntry):
            self.assertIsNone(service.create_trace_entry_from_dict(trace_id, "INVALID"))

    def test_store_trace_entries(self):
        trace_id = "123TEST123"
        data = [
            {
                "timestamp": 1569972082,
                "type": "TRACE_START",
                "group_id": "e6fa79ca-d142-4046-a134-5134f16a0b5e",
            },
            {
                "timestamp": 1569972084,
                "type": "SPAN_START",
                "group_id": "dbed4e4e-8ec6-40dd-ae35-b118abf1ab69",
            },
        ]

        processed = service.store_trace_entries(trace_id, data)
        self.assertEqual(processed, True)

    def test_store_trace_entries_invalid_timestamp(self):
        trace_id = "123TEST123"
        data = [
            {
                "timestamp": None,
                "type": "TRACE_START",
                "group_id": "e6fa79ca-d142-4046-a134-5134f16a0b5e",
            },
            {
                "timestamp": 1569972084,
                "type": "SPAN_START",
                "group_id": "dbed4e4e-8ec6-40dd-ae35-b118abf1ab69",
            },
        ]

        processed = service.store_trace_entries(trace_id, data)
        self.assertEqual(processed, False)

    def test_store_trace_entries_invalid_type(self):
        trace_id = "123TEST123"
        data = [
            {
                "timestamp": 1569972082,
                "type": "TRACE_START",
                "group_id": "e6fa79ca-d142-4046-a134-5134f16a0b5e",
            },
            {
                "timestamp": 1569972084,
                "type": None,
                "group_id": "dbed4e4e-8ec6-40dd-ae35-b118abf1ab69",
            },
        ]

        processed = service.store_trace_entries(trace_id, data)
        self.assertEqual(processed, False)

    def test_archive_trace_entries(self):
        trace_id = "123TEST123_ARCHIVED"
        mixer.blend(models.TraceEntry, trace_id=trace_id, type=constants.TraceType.TRACE_START, timestamp=1569972082, group_id="e6fa79ca-d142-4046-a134-5134f16a0b5e")
        mixer.blend(models.TraceEntry, trace_id=trace_id, type=constants.TraceType.SPAN_END, timestamp=1569972090, group_id="dbed4e4e-8ec6-40dd-ae35-b118abf1ab69")

        trace_entries = list(models.TraceEntry.objects.filter(trace_id=trace_id))
        self.assertEqual(len(trace_entries), 2)
        for trace_entry in trace_entries:
            self.assertEqual(trace_entry.archived, False)

        service.archive_trace_entries(trace_entries)
        for trace_entry in trace_entries:
            self.assertEqual(trace_entry.archived, True)
