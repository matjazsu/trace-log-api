from common.views_base import BaseView
from . import service


class TraceLogView(BaseView):
    def get(self, request, trace_id):
        trace_log = service.get_trace_log(trace_id)
        return self.create_api_response(trace_log.trace_data)
