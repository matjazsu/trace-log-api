from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from common.errors_base import MissingDataError

from . import TraceLog


def get_trace_log(trace_id) -> TraceLog:
    try:
        return TraceLog.objects.get(trace_id=trace_id)
    except ObjectDoesNotExist:
        raise MissingDataError("Missing trace log")


@transaction.atomic()
def create_trace_log_from_dict(trace_id, trace_dict):
    item = TraceLog(trace_id=trace_id, trace_data=trace_dict)
    item.save()
    return item
