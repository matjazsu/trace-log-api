from django.urls import path
from . import views

urlpatterns = [
    path("<str:trace_id>", views.TraceLogView.as_view(), name="trace_log_view"),
]
