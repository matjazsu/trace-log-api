from django.test import TestCase
from mixer.backend.django import mixer
from common.errors_base import MissingDataError

from . import service
from . import models


class ServiceTestCase(TestCase):
    def test_get_trace_log(self):
        trace_id = "123TEST123"
        trace_data = [{
            "type": "TRACE",
            "start": 1569972082,
            "end": 1569972090,
            "children": []
        }]
        mixer.blend(models.TraceLog, trace_id=trace_id, trace_data=trace_data)
        trace_log = service.get_trace_log(trace_id)
        self.assertIsNotNone(trace_log)
        self.assertEqual(trace_log.trace_id, trace_id)
        self.assertEqual(trace_log.trace_data, trace_data)

    def test_get_trace_log_invalid(self):
        with self.assertRaises(MissingDataError):
            service.get_trace_log(None)
        with self.assertRaises(MissingDataError):
            service.get_trace_log(123)
        with self.assertRaises(MissingDataError):
            service.get_trace_log("123")

    def test_create_trace_log_from_dict(self):
        trace_id = "123TEST123"
        trace_data = [{
            "type": "TRACE",
            "start": 1569972082,
            "end": 1569972090,
            "children": []
        }]

        trace_log = service.create_trace_log_from_dict(trace_id, trace_data)
        self.assertIsNotNone(trace_log)
        self.assertEqual(trace_log.trace_id, trace_id)
        self.assertEqual(trace_log.trace_data, trace_data)
