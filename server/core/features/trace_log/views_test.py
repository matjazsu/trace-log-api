import json

from django.test import TestCase
from django.test import Client
from django.urls import reverse
from mixer.backend.django import mixer

from . import TraceLog


class TraceLogViewTestCase(TestCase):
    def setUp(self) -> None:
        super().setUp()
        self.client = Client()

    def test_get(self):
        trace_id = "123TEST123"
        trace_data = [{
            "type": "TRACE",
            "start": 1569972082,
            "end": 1569972090,
            "children": []
        }]
        mixer.blend(TraceLog, trace_id=trace_id, trace_data=trace_data)
        response = self.client.get(reverse("trace_log_view", kwargs={"trace_id": trace_id}))
        self.assertEqual(200, response.status_code)
        decoded_response = json.loads(response.content)
        self.assertTrue(decoded_response["success"])
        self.assertEqual(decoded_response["data"], trace_data)

    def test_get_not_found(self):
        response = self.client.get(reverse("trace_log_view", kwargs={"trace_id": "TEST123"}))
        self.assertEqual(404, response.status_code)
        decoded_response = json.loads(response.content)
        self.assertFalse(decoded_response["success"])
        self.assertEqual(decoded_response["data"], {
            "error_code": "MissingDataError",
            "message": "Missing trace log",
        })
