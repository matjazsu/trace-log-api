from django.db import models


class TraceLog(models.Model):
    trace_id = models.CharField(max_length=256, unique=True)
    trace_data = models.JSONField()
    objects = models.Manager
