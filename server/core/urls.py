from django.conf.urls import include
from django.urls import path


urlpatterns = [
    path("trace/", include('core.features.trace_log.urls')),
]
