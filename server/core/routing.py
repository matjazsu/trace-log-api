from django.urls import path

from .features.trace_entry import TraceEntryConsumer

websocket_urlpatterns = [
    path("api/trace/<str:trace_id>", TraceEntryConsumer),
]
