from django.test import TestCase
from mixer.backend.django import mixer

import core.models
from core.features.trace_entry.constants import TraceType

from . import service


class ServiceTestCase(TestCase):
    def setUp(self) -> None:
        super().setUp()
        self.maxDiff = None

    def test_materialize_trace_id(self):
        trace_id = "777888999"

        mixer.blend(core.models.TraceEntry, trace_id=trace_id, type=TraceType.TRACE_START, timestamp=1569972082, group_id="e6fa79ca-d142-4046-a134-5134f16a0b5e")
        mixer.blend(core.models.TraceEntry, trace_id=trace_id, type=TraceType.SPAN_END, timestamp=1569972090, group_id="dbed4e4e-8ec6-40dd-ae35-b118abf1ab69")
        mixer.blend(core.models.TraceEntry, trace_id=trace_id, type=TraceType.SPAN_START, timestamp=1569972084, group_id="1ff14a38-55ff-431a-a6ca-a86c82d8ed46", name="movies_network_load")
        mixer.blend(core.models.TraceEntry, trace_id=trace_id, type=TraceType.SPAN_END, timestamp=1569972090, group_id="1ff14a38-55ff-431a-a6ca-a86c82d8ed46")
        mixer.blend(core.models.TraceEntry, trace_id=trace_id, type=TraceType.EVENT, timestamp=1569972083, name="movies_cache_content_rendered")
        mixer.blend(core.models.TraceEntry, trace_id=trace_id, type=TraceType.SPAN_START, timestamp=1569972082, group_id="dbed4e4e-8ec6-40dd-ae35-b118abf1ab69", name="movies_load")
        mixer.blend(core.models.TraceEntry, trace_id=trace_id, type=TraceType.TRACE_END, timestamp=1569972090, group_id="e6fa79ca-d142-4046-a134-5134f16a0b5e")

        trace_entries = list(core.models.TraceEntry.objects.filter(trace_id=trace_id))
        self.assertEqual(len(trace_entries), 7)
        for trace_entry in trace_entries:
            self.assertEqual(trace_entry.archived, False)

        service.materialize_trace_id(trace_id)

        trace_entries = list(core.models.TraceEntry.objects.filter(trace_id=trace_id))
        self.assertEqual(len(trace_entries), 7)
        for trace_entry in trace_entries:
            self.assertEqual(trace_entry.archived, True)

        trace_log = core.models.TraceLog.objects.filter(trace_id=trace_id).first()
        self.assertIsNotNone(trace_log)
        self.assertIsNotNone(trace_log.trace_data)
        self.assertDictEqual(trace_log.trace_data, {
            "type": "TRACE",
            "start": 1569972082,
            "end": 1569972090,
            "children": [
                {
                    "type": "SPAN",
                    "start": 1569972082,
                    "end": 1569972090,
                    "name": "movies_load",
                    "children": [
                        {
                            "type": "EVENT",
                            "timestamp": 1569972083,
                            "name": "movies_cache_content_rendered"
                        },
                        {
                            "type": "SPAN",
                            "start": 1569972084,
                            "end": 1569972090,
                            "name": "movies_network_load"
                        }
                    ]
                }
            ]
        })
