from django.apps import AppConfig


class EtlConfig(AppConfig):
    name = 'etl'

    def ready(self):
        from . import scheduler
        scheduler.start()
