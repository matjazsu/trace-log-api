import concurrent.futures
import intervaltree
import core.models
import core.features.trace_entry.constants
import core.features.trace_log.service
import core.features.trace_entry.service

MAX_NUM_WORKERS = 8


def materialize():
    trace_ids = list(core.models.TraceEntry.objects.filter(archived=False).values_list("trace_id", flat=True).distinct())
    with concurrent.futures.ThreadPoolExecutor(max_workers=MAX_NUM_WORKERS) as executor:
        futures = [executor.submit(materialize_trace_id, trace_id) for trace_id in trace_ids]
        concurrent.futures.wait(futures)


def materialize_trace_id(trace_id):
    entries = list(core.models.TraceEntry.objects.filter(archived=False, trace_id=trace_id))
    if not _are_entries_complete(entries):
        return
    _materialize_entries(trace_id, entries)


def _are_entries_complete(entries):
    return (
        len(entries) >= 2 and
        any(x.type == core.features.trace_entry.constants.TraceType.TRACE_START for x in entries) and
        any(x.type == core.features.trace_entry.constants.TraceType.TRACE_END for x in entries)
    )


def _materialize_entries(trace_id, entries):
    trace_dict = dict()
    _process_trace_entries(trace_dict, entries)
    _process_span_entries(trace_dict, entries)
    _process_event_entries(trace_dict, entries)

    core.features.trace_log.service.create_trace_log_from_dict(trace_id, trace_dict)
    core.features.trace_entry.service.archive_trace_entries(entries)


def _process_trace_entries(root, entries):
    root["type"] = "TRACE"
    for trace in [x for x in entries if x.type == core.features.trace_entry.constants.TraceType.TRACE_START or x.type == core.features.trace_entry.constants.TraceType.TRACE_END]:
        if trace.type == core.features.trace_entry.constants.TraceType.TRACE_START:
            root["start"] = trace.timestamp
        else:
            root["end"] = trace.timestamp


def _process_span_entries(root, entries):
    spans = [x for x in entries if x.type == core.features.trace_entry.constants.TraceType.SPAN_START or x.type == core.features.trace_entry.constants.TraceType.SPAN_END]
    if len(spans) == 0:
        return

    spans_cache = dict()
    for span in spans:
        if spans_cache.get(span.group_id) is None:
            spans_cache[span.group_id] = dict()
            spans_cache[span.group_id]["type"] = "SPAN"
        if span.type == core.features.trace_entry.constants.TraceType.SPAN_START:
            spans_cache[span.group_id]["start"] = span.timestamp
            spans_cache[span.group_id]["name"] = span.name if span.name is not None else ""
        else:
            spans_cache[span.group_id]["end"] = span.timestamp

    tree = intervaltree.IntervalTree()
    for key, value in spans_cache.items():
        tree[value["start"]:value["end"]] = value

    processed_intervals = []
    for interval in sorted(tree):
        if interval in processed_intervals:
            continue
        _process_interval(root, interval, tree, processed_intervals)


def _process_interval(root, interval, tree, processed_intervals):
    if not root.get("children"):
        root["children"] = []
    root["children"].append(interval.data)

    overlap_intervals = list(sorted(tree[interval.begin:interval.end]))
    children_intervals = overlap_intervals[overlap_intervals.index(interval)+1:len(overlap_intervals)]
    for child_interval in children_intervals:
        if child_interval in processed_intervals:
            continue
        _process_interval(interval.data, child_interval, tree, processed_intervals)
        processed_intervals.append(child_interval)


def _process_event_entries(root, entries):
    events = [x for x in entries if x.type == core.features.trace_entry.constants.TraceType.EVENT]
    if len(events) == 0:
        return

    items = [{
        "type": "EVENT",
        "timestamp": event.timestamp,
        "name": event.name if event.name is not None else "",
    } for event in events]

    for item in items:
        _insert_event(item, root)


def _insert_event(event, root):
    if not root.get("children"):
        root["children"] = []
        root["children"].append(event)
        return
    for child in root.get("children"):
        if child["type"] == "EVENT":
            if event["timestamp"] <= child["timestamp"]:
                root["children"].insert(root["children"].index(child), event)
                return
        else:
            if event["timestamp"] <= child["start"]:
                root["children"].insert(root["children"].index(child), event)
                return
            elif child["start"] < event["timestamp"] < child["end"]:
                _insert_event(event, child)
                return
    root["children"].append(event)
