from apscheduler.schedulers.background import BackgroundScheduler
from . import service


def start():
    scheduler = BackgroundScheduler()
    scheduler.add_job(service.materialize, 'interval', minutes=1)
    scheduler.start()
