from django.core.management.base import BaseCommand
from etl import service


class Command(BaseCommand):
    def handle(self, *args, **options):
        service.materialize()
