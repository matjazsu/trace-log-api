class BaseError(Exception):
    def __init__(self, message=None, pretty_message=None):
        super(BaseError, self).__init__(message)
        self.error_code = self.__class__.__name__
        self.http_status_code = 500
        self.pretty_message = pretty_message


class MissingDataError(BaseError):
    def __init__(self, message=None, pretty_message=None):
        super(MissingDataError, self).__init__(message, pretty_message)
        self.http_status_code = 404
