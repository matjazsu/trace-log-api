import json

from django.views.generic import View
from django.http import HttpResponse

from .errors_base import MissingDataError

CUSTOM_ERRORS = (MissingDataError,)


class BaseView(View):
    @staticmethod
    def create_api_response(data=None, success=True, status_code=200):
        body = {"success": success}
        if data:
            body["data"] = data
        response = HttpResponse(content=json.dumps(body), content_type="application/json", status=status_code)
        return response

    def get_exception_response(self, exception):
        error_data = {}
        if any(isinstance(exception, custom_error_cls) for custom_error_cls in CUSTOM_ERRORS):
            error_data["error_code"] = exception.error_code
            error_data["message"] = exception.pretty_message or exception.args[0]
            status_code = exception.http_status_code
        else:
            error_data["error_code"] = "ServerError"
            error_data["message"] = "An error occurred."
            status_code = 500

        return self.create_api_response(error_data, success=False, status_code=status_code)

    def dispatch(self, request, *args, **kwargs):
        try:
            response = super(BaseView, self).dispatch(request, *args, **kwargs)
            return response
        except Exception as exc:
            response = self.get_exception_response(exc)
            return response
